﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

public class ShadowsOnOff : MonoBehaviour {


	[MenuItem("AlonTools/Shadows cast and receive OFF")]
	static void ShadowsCastAndReceiveOff(){
		foreach(Transform transform in Selection.transforms){
			MeshRenderer mR = transform.GetComponent<MeshRenderer>();
			MeshRenderer[] mRs = transform.GetComponentsInChildren<MeshRenderer>();

			if (mR != null) {
				mR.shadowCastingMode = ShadowCastingMode.Off;
				mR.receiveShadows = false;
			}

			if (mRs != null && mRs.Length > 0) {
				foreach(MeshRenderer tempMR in mRs){
					tempMR.shadowCastingMode = ShadowCastingMode.Off;
					tempMR.receiveShadows = false;
				}
			}
		}
	}


	[MenuItem("AlonTools/Shadows cast and receive ON")]
	static void ShadowsCastAndReceiveOn(){
		foreach(Transform transform in Selection.transforms){
			MeshRenderer mR = transform.GetComponent<MeshRenderer>();
			MeshRenderer[] mRs = transform.GetComponentsInChildren<MeshRenderer>();

			if (mR != null) {
				mR.shadowCastingMode = ShadowCastingMode.On;
				mR.receiveShadows = true;
			}

			if (mRs != null && mRs.Length > 0) {
				foreach(MeshRenderer tempMR in mRs){
					tempMR.shadowCastingMode = ShadowCastingMode.On;
					tempMR.receiveShadows = true;
				}
			}
		}
	}


	[MenuItem("AlonTools/Shadows cast OFF")]
	static void ShadowsCastOff(){
		foreach(Transform transform in Selection.transforms){
			MeshRenderer mR = transform.GetComponent<MeshRenderer>();
			MeshRenderer[] mRs = transform.GetComponentsInChildren<MeshRenderer>();

			if (mR != null) {
				mR.shadowCastingMode = ShadowCastingMode.Off;
			}

			if (mRs != null && mRs.Length > 0) {
				foreach(MeshRenderer tempMR in mRs){
					tempMR.shadowCastingMode = ShadowCastingMode.Off;
				}
			}
		}
	}


	[MenuItem("AlonTools/Shadows cast ON")]
	static void ShadowsCastOn(){
		foreach(Transform transform in Selection.transforms){
			MeshRenderer mR = transform.GetComponent<MeshRenderer>();
			MeshRenderer[] mRs = transform.GetComponentsInChildren<MeshRenderer>();

			if (mR != null) {
				mR.shadowCastingMode = ShadowCastingMode.On;
			}

			if (mRs != null && mRs.Length > 0) {
				foreach(MeshRenderer tempMR in mRs){
					tempMR.shadowCastingMode = ShadowCastingMode.On;
				}
			}
		}
	}


	[MenuItem("AlonTools/Shadows receive OFF")]
	static void ShadowsReceiveOff(){
		foreach(Transform transform in Selection.transforms){
			MeshRenderer mR = transform.GetComponent<MeshRenderer>();
			MeshRenderer[] mRs = transform.GetComponentsInChildren<MeshRenderer>();

			if (mR != null) {
				mR.receiveShadows = false;
			}

			if (mRs != null && mRs.Length > 0) {
				foreach(MeshRenderer tempMR in mRs){
					tempMR.receiveShadows = false;
				}
			}
		}
	}


	[MenuItem("AlonTools/Shadows receive ON")]
	static void ShadowsReceiveOn(){
		foreach(Transform transform in Selection.transforms){
			MeshRenderer mR = transform.GetComponent<MeshRenderer>();
			MeshRenderer[] mRs = transform.GetComponentsInChildren<MeshRenderer>();

			if (mR != null) {
				mR.receiveShadows = true;
			}

			if (mRs != null && mRs.Length > 0) {
				foreach(MeshRenderer tempMR in mRs){
					tempMR.receiveShadows = true;
				}
			}
		}
	}




	[MenuItem("AlonTools/How Much Selected")]
	static void HowMuchSelected(){
		int countIndex = 0;
		int childrenCountIndex = 0;
		foreach(Transform transform in Selection.transforms){
			childrenCountIndex += transform.childCount;
			countIndex++;
		}
		Debug.Log ("Total gameobjects selected: " + countIndex);
		Debug.Log ("The count of the children of the selected objects: " + childrenCountIndex);
	}






}
