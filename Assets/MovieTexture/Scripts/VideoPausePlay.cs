﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPausePlay : MonoBehaviour {

	public MediaPlayerCtrl mediaPlayerCtrl;

	bool videoPaused = false;

	public GameObject playBtn;


	void OnMouseDown(){
		videoPaused = !videoPaused;
		if (!videoPaused) {
			Debug.Log ("Video should be played");
			mediaPlayerCtrl.Play ();
			playBtn.SetActive (false);
		} else {
			Debug.Log ("Video should be stoped");
			mediaPlayerCtrl.Pause ();
			playBtn.SetActive (true);
		}
	}


}
