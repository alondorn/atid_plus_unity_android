﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoTargetManager : MonoBehaviour {

	public GameObject targetlight;
	public MediaPlayerCtrl mediaPlayerCtrl;


	public void StartEvent () {
		targetlight.SetActive (true);
		mediaPlayerCtrl.Play ();
	}
	
	public void EndEvent () {
		targetlight.SetActive (false);
		mediaPlayerCtrl.Pause ();
	}
}
