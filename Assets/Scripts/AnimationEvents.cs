﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour {

	public void WheelsDown(){
		PlaneManager.instance.wheelsDown = true;
		PlaneManager.instance.wheelsMoving = false;
	}
	
	public void WheelsUp(){
		PlaneManager.instance.wheelsDown = false;
		PlaneManager.instance.wheelsMoving = false;
	}

	public void WheelsMoving(){
		PlaneManager.instance.wheelsMoving = true;
	}
}
