﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class InfoPopup : MonoBehaviour {

	public static Animation infoPopupAnim;

	public static Text infoPopupText;
	public static Text toggleBtnText;

	public static RawImage infoPopupImage;

	public static RectTransform infoContentRectTransform;

	public static ScrollRect infoScrollRect;

	public static Vector2 infoContentSize;
	public static Vector2 imageContentSize;

	//public static Vector3 imageContentStartPosition;

	static string nextText;

	static Texture2D imageTexture;

	static bool flipText = false;
	static bool isDown = true;
	static bool toggleModeOn = false;

	static int nextFontSize = 57;

	static char[] charArray;




	
	public static void SwitchPopup(string content = "" , bool flip = false , int fontSize = 57 , Texture2D image = null){
		toggleModeOn = false;

		nextText = content;
		nextFontSize = fontSize;
		flipText = flip;
		imageTexture = image;

		if (isDown) {
			ShowPopup ();
		} else {
			infoPopupAnim.Play ("popup_hide");
			toggleBtnText.text = ReverseString("הצג מידע");
		}
	}


	public static void ShowPopup(){
		if (toggleModeOn)
			return;

		if (nextText == "") {
			isDown = true;
			return;
		}
		Debug.Log ("should reset scroller, infoScrollRect.verticalNormalizedPosition: " + infoScrollRect.verticalNormalizedPosition);
		//infoContentRectTransform.position = imageContentStartPosition;
		infoScrollRect.verticalNormalizedPosition = 1;

		if (flipText) {
			nextText = ReverseString (nextText);
			infoPopupText.lineSpacing = -(Mathf.Abs(infoPopupText.lineSpacing));
		}

		infoPopupText.text = nextText;
		infoPopupText.fontSize = nextFontSize;

		if (imageTexture == null) {
			infoPopupImage.gameObject.SetActive (false);
			infoContentRectTransform.sizeDelta = infoContentSize;
		} else {
			infoPopupImage.gameObject.SetActive (true);
			infoPopupImage.texture = imageTexture;
			infoContentRectTransform.sizeDelta = imageContentSize;
		}

		infoPopupAnim.Play ("popup_show");
		toggleBtnText.text = ReverseString("הסתר מידע");
		isDown = false;
	}


	public static void PopupToggle(){
		toggleModeOn = true;
		if (isDown) {
			infoPopupAnim.Play ("popup_show");
			isDown = false;
			toggleBtnText.text = ReverseString("הסתר מידע");
		} else {
			infoPopupAnim.Play ("popup_hide");
			isDown = true;
			toggleBtnText.text = ReverseString("הצג מידע");
		}
	}



	public static string ReverseString(string s)
	{
		charArray = s.ToCharArray();
		Array.Reverse(charArray);
		return new string(charArray);
	}


}
