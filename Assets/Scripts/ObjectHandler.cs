﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;

public class ObjectHandler : MonoBehaviour {
	
	private float _sensitivity;
	private Vector3 _mouseReference;
	private Vector3 _mouseOffset;
	private Vector3 _rotation;
	private bool _isRotating = false;
	public bool isHiden = false;
	public bool hasButton = false;
	public bool isActive = false;
	private Quaternion mainRotation;
	private Vector3 mainPosition;
	public float zoomSize = 1;
	public Transform targetPosition;

//	public AppManager.ObjectType currentObjectType = AppManager.ObjectType.BRAIN;
//	public delegate void MDelegate();
//	public MDelegate InfoPopupMethod;

	public UnityEvent InfoPopupEvent;


	//public List<GameObject> objsToHide;


	
	void Start ()
	{
		_sensitivity = 0.3f;
		_rotation = Vector3.zero;
		if (targetPosition == null) {
			targetPosition = transform;
		}

	}

	void OnEnable() {
		mainRotation = transform.rotation;
		mainPosition = transform.localPosition;
		//objsToHide = GetComponentInParent<ObjectsManager> ().objectsToHide;
	}
	void OnDisable() {
		transform.rotation = mainRotation;
		transform.localPosition = mainPosition;
	}
	
	void FixedUpdate()
	{
		if (isActive && _isRotating) {
			_mouseOffset = (Input.mousePosition - _mouseReference);

			_rotation.x = (_mouseOffset.y) * _sensitivity;
			_rotation.y = (_mouseOffset.x) * _sensitivity;

			transform.RotateAround (transform.position, Vector3.down, _rotation.y);
			transform.RotateAround (transform.position, Vector3.right, _rotation.x);

			_mouseReference = Input.mousePosition;
		}
	}
	
	void OnMouseDown()
	{
		if (!isActive)
			return;
		
		_mouseReference = Input.mousePosition;

		_isRotating = true;

	}
	
	void OnMouseUp()
	{
		if (!isActive) {
			StartCoroutine(AppManager.instace.ObjectZoomIn (transform, zoomSize, targetPosition.position , this));
		} else {
			_isRotating = false;
		}
	}

	public GameObject zoomBtn;
	public void ZoomBtn(){
		StartCoroutine(AppManager.instace.ObjectZoomIn (transform, zoomSize, targetPosition.position , this));

	}
		

	public void ActivateObject(){
		isActive = true;
	}

	public void DeactivateObject(){
		isActive = false;
	}


	public void ShowInfoPopup(){
		InfoPopupEvent.Invoke ();
	}


	public void ShowChildren(){
		if (isHiden) {
			foreach (Transform child in transform) {
				child.gameObject.SetActive (true);
			}
		}
		if (hasButton) {
			zoomBtn.GetComponent<BoxCollider> ().enabled = false;
			zoomBtn.GetComponent<Renderer> ().enabled = false;
			//zoomBtn.transform.FindChild ("shadowcaster").gameObject.SetActive (false);
		}
	}

	public void HideChildren(){
		if (isHiden) {
			foreach (Transform child in transform) {
				child.gameObject.SetActive (false);
			}
		}
		if (hasButton) {
			zoomBtn.GetComponent<BoxCollider> ().enabled = true;
			zoomBtn.GetComponent<Renderer> ().enabled = true;
			//zoomBtn.transform.FindChild ("shadowcaster").gameObject.SetActive (true);
		}
	}



//	public void HideObjects(GameObject mainObj){
//		foreach (GameObject obj in objsToHide) {
//			if (obj != mainObj) {
//				obj.SetActive (false);
//			}
//		}
//	}

//	public void ShowObjects(){
//		foreach (GameObject obj in objsToHide) {
//			obj.SetActive (true);
//		}
//	}

	
}