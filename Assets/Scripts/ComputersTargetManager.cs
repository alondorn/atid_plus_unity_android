﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputersTargetManager : MonoBehaviour {

	public GameObject targetlight;


	public void StartEvent () {
		targetlight.SetActive (true);
	}

	public void EndEvent () {
		targetlight.SetActive (false);
	}
}
