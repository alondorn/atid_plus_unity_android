﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsManager : MonoBehaviour {


	public List<GameObject> objectsToHide;


		public void HideObjects(GameObject mainObj){
		foreach (GameObject obj in objectsToHide) {
			if (obj != mainObj && obj != mainObj.transform.parent.gameObject) {
					obj.SetActive (false);
				}
			}
	}

		public void ShowObjects(){
		foreach (GameObject obj in objectsToHide) {
				obj.SetActive (true);
			}
	}

}
