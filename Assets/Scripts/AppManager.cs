﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using ADTools;
using UnityEngine.UI;

public class AppManager : MonoBehaviour {
	
	public static AppManager instace;
	//public Transform standardPosition;

	WaitForFixedUpdate waitFixedUpdate = new WaitForFixedUpdate ();
	float lerpStep;
	float lerpIncreaseFactor = 0.01f;
	Transform activeObject;
	Vector3 currentOriginalSize;
	Vector3 currentOriginalPosition;
	Vector3 currentOriginalRotation;
	Vector3 currentTargetSize;
	//Vector3 standardPositionValue;

	ObjectHandler currentObjHandler;

	public GameObject objZoomOutBtn;

	public enum ObjectType {BRAIN , HEART};





	void Awake(){
		instace = this;
		//standardPositionValue = standardPosition.position;
	}

	#if UNITY_EDITOR
	void Update(){
		if (Input.GetKeyUp (KeyCode.Backspace)) {
			StartCoroutine (ObjectZoomOut());
		}
	}
	#endif




	public IEnumerator ObjectZoomIn(Transform obj , float targetSize , Vector3 targetPos , ObjectHandler objHandler){
		obj.GetComponentInParent<ObjectsManager> ().HideObjects (obj.gameObject);
		currentOriginalSize = obj.localScale;
		currentOriginalPosition = obj.position;
		currentTargetSize.x = currentTargetSize.y = currentTargetSize.z = targetSize;
		currentOriginalRotation = obj.eulerAngles;
		currentObjHandler = objHandler;
		objHandler.ShowChildren ();
		objHandler.ShowInfoPopup ();
		lerpStep = 0;
		while (lerpStep < 0.3f) {
			lerpStep += lerpIncreaseFactor;
			obj.localScale = Vector3.Lerp (obj.localScale, currentTargetSize, lerpStep);
			obj.position = Vector3.Lerp (obj.position, targetPos, lerpStep);
			yield return waitFixedUpdate;
		}
		obj.localScale = currentTargetSize;
		obj.position = targetPos;
		activeObject = obj;
		objHandler.ActivateObject ();

		objZoomOutBtn.SetActive (true);
	}



	public void objZoomOut(){
		StartCoroutine (ObjectZoomOut());
		objZoomOutBtn.SetActive (false);
		InfoPopup.SwitchPopup ();
	}

	public IEnumerator ObjectZoomOut(){
		if (activeObject == null)
			yield break;
		
		lerpStep = 0;
		while (lerpStep < 0.3f) {
			lerpStep += lerpIncreaseFactor;
			activeObject.localScale = Vector3.Lerp (activeObject.localScale, currentOriginalSize, lerpStep);
			activeObject.position = Vector3.Lerp (activeObject.position, currentOriginalPosition, lerpStep);
			activeObject.eulerAngles = Vector3.Lerp (activeObject.eulerAngles, currentOriginalRotation, lerpStep);
			yield return waitFixedUpdate;
		}
		activeObject.localScale = currentOriginalSize;
		activeObject.position = currentOriginalPosition;
		activeObject.eulerAngles = currentOriginalRotation;
		currentObjHandler.DeactivateObject ();
		activeObject.GetComponentInParent<ObjectsManager> ().ShowObjects ();
		currentObjHandler.HideChildren ();
	}

	public void ResetObject(){
		if (activeObject == null)
			return;
		
		activeObject.localScale = currentOriginalSize;
		activeObject.position = currentOriginalPosition;
		activeObject.eulerAngles = currentOriginalRotation;
		activeObject.GetComponent<ObjectHandler> ().DeactivateObject ();
		//activeObject.GetComponent<ObjectHandler>().ShowObjects ();
		activeObject.GetComponentInParent<ObjectsManager> ().ShowObjects ();
		instace.objZoomOutBtn.SetActive (false);
	}



	public GameObject targetsBtn;
	public Animator targetsScrbAnim;
	bool targetsScrnOn = false;
	public void TargetsScreenToggle(bool on){
		if (on && !targetsScrnOn) {
			targetsScrbAnim.Play ("targets_scrn_in");
			targetsScrnOn = true;
		} else if (!on && targetsScrnOn) {
			targetsScrbAnim.Play ("targets_scrn_out");
			targetsScrnOn = false;
		}
	}

	public void OpenAnatomyLink(){
		Application.OpenURL ("https://alondorn-puzzle.000webhostapp.com/Wiki360_targets/Target_anatomy_small.jpg");
	}

	public void OpenAtidPlusLink(){
		Application.OpenURL ("https://alondorn-puzzle.000webhostapp.com/Wiki360_targets/Target_AtidPlus_small.jpg");
	}

	public void OpenComputersLink(){
		Application.OpenURL ("https://alondorn-puzzle.000webhostapp.com/Wiki360_targets/Target_computer_evolution_small.jpg");
	}

	public void OpenPlaneLink(){
		Application.OpenURL ("https://alondorn-puzzle.000webhostapp.com/Wiki360_targets/Target_F35_small.jpg");
	}




	public void ChechIfObjIsActive(){
		if (currentObjHandler != null) {
			if (currentObjHandler.isActive) {
				objZoomOutBtn.SetActive (true);
				activeObject.GetComponentInParent<ObjectsManager> ().HideObjects (activeObject.gameObject);
			}
		}
	}


}
