﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpriteButton : MonoBehaviour {

	public float moveAmount = 0.2f;
	float stepFactor = 0;

	WaitForEndOfFrame waitFrame = new WaitForEndOfFrame ();

	Vector3 currentPosition;
	Vector3 targetPosition;

	public UnityEvent buttonEvent;

	bool onAction = false;






	void OnMouseDown(){
		//Debug.Log ("sprite pressed!");
		//StartCoroutine (ButtonPressed ());
		ButtonPressed ();
	}


	void ButtonPressed(){
		buttonEvent.Invoke ();
	}



	IEnumerator ButtonPressedAnim(){
		if (onAction)
			yield break;
		onAction = true;
		currentPosition = targetPosition = transform.localPosition;
		targetPosition.y -= moveAmount;
		while (stepFactor < 0.75f) {
			transform.localPosition = Vector3.Lerp (transform.localPosition, targetPosition, stepFactor);
			stepFactor += 0.05f;
			yield return waitFrame;
		}
		transform.localPosition = targetPosition;
		stepFactor = 0;

		buttonEvent.Invoke ();

		while (stepFactor < 0.75f) {
			transform.localPosition = Vector3.Lerp (transform.localPosition, currentPosition, stepFactor);
			stepFactor += 0.05f;
			yield return waitFrame;
		}
		transform.localPosition = currentPosition;
		stepFactor = 0;
		onAction = false;
	}
}
