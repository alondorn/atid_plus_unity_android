﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TargetStartHandler : MonoBehaviour {

	WaitForSeconds waitSec = new WaitForSeconds(1);

	public Animation anim;

	bool shouldRestart = true;
	bool started = false;

	int secondsIndex = 0;
	public int timeToRestart = 10;

	public UnityEvent TargetStartEvent;
	public UnityEvent TargetEndEvent;


	public IEnumerator TargetCountDown(){

		if (!started)
			yield break;

		started = false;
		PopupsManager.instance.StartPopup ();
		TargetEndEvent.Invoke ();

		if (shouldRestart)
			yield break;

		secondsIndex = 0;
		while (!started && secondsIndex < timeToRestart) {
			yield return waitSec;
			secondsIndex++;
			Debug.Log ("Count down: " + secondsIndex);
		}
		if (!started)
			shouldRestart = true;
	}


	public void StartTargetEvent(){
		if (started)
			return;

		started = true;

		if (shouldRestart) {
			anim.Play ();
			AppManager.instace.ResetObject ();
			shouldRestart = false;
		} else {
			AppManager.instace.ChechIfObjIsActive ();
		}

		InfoPopup.SwitchPopup ();
		TargetStartEvent.Invoke ();
	}
}
