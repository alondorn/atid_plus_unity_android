﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDoor : MonoBehaviour {

	private float _sensitivity;
	private Vector3 _mouseReference;
	private Vector3 _mouseOffset;
	private Vector3 _rotation;
	Quaternion qRotation;
	private bool _isRotating;

	void Start ()
	{
		_sensitivity = 0.3f;
		_rotation = Vector3.zero;
	}

	void FixedUpdate()
	{
		if(_isRotating)
		{
			if (transform.rotation.eulerAngles.y <= 180 && transform.rotation.eulerAngles.y >= 95) {
				
				_mouseOffset = (Input.mousePosition - _mouseReference);

				_rotation.y = -_mouseOffset.x * _sensitivity;

				transform.Rotate (_rotation);

				_mouseReference = Input.mousePosition;
			}else if (transform.rotation.eulerAngles.y > 180){
				_rotation.y = 180;
				qRotation.eulerAngles = _rotation;
				transform.rotation = qRotation;
				_isRotating = false;
			}else if (transform.rotation.eulerAngles.y < 95){
				_rotation.y = 95.1f;
				qRotation.eulerAngles = _rotation;
				transform.rotation = qRotation;
				_isRotating = false;
			}
		}
	}

	void OnMouseDrag()
	{
		_mouseReference = Input.mousePosition;
		_isRotating = true;
	}

	void OnMouseUp()
	{
		_isRotating = false;
		//Debug.Log ("transform.rotation.y: " + transform.rotation.eulerAngles.y);
	}
}
