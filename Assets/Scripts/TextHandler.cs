﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TextHandler : MonoBehaviour {

	string myString;
	string reversedString;
	Text text;
	char[] charArray;


	void Start () {
		text = GetComponent<Text> ();
		myString = text.text;
		reversedString = ReverseString (myString);
		text.text = reversedString;
		text.lineSpacing = -(Mathf.Abs(text.lineSpacing));
	}

	public string ReverseString(string s)
	{
		charArray = s.ToCharArray();
		Array.Reverse(charArray);
		return new string(charArray);
	}


	

}
