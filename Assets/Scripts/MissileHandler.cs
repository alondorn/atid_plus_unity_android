﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileHandler : MonoBehaviour {


	public Animation anim;

	public ParticleSystem particles;

	public Transform missileTrnsfrm;

	Vector3 missileStartPosition;





	void Start(){
		missileStartPosition = missileTrnsfrm.localPosition;
	}




	public void Lounch () {
		particles.Play ();
		anim.Play ();
	}
	



	public void Reset () {
		Debug.Log ("Reset ()");
		particles.Stop ();
		anim.Stop ();
		//anim.Rewind ();
		anim.enabled = false;
		missileTrnsfrm.localPosition = missileStartPosition;
		anim.enabled = true;
	}
}
