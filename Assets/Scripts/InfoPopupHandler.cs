﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using ADTools;


public class InfoPopupHandler : MonoBehaviour {

	Animation anim;

	public Text text;
	public Text toggleBtnTxt;

	public RawImage image;

	public RectTransform contentRectTransform;

	public ScrollRect scrollRect;

	//Vector3 contentStartPosition;






	void Awake () {
		anim = GetComponent<Animation> ();
		//contentStartPosition = contentRectTransform.position;

		InfoPopup.infoPopupAnim = anim;
		InfoPopup.infoPopupText = text;
		InfoPopup.infoPopupImage = image;
		InfoPopup.infoContentRectTransform = contentRectTransform;
		InfoPopup.infoContentSize = InfoPopup.imageContentSize = contentRectTransform.sizeDelta;
		InfoPopup.imageContentSize.x -= image.GetComponent<RectTransform> ().sizeDelta.x;
		//InfoPopup.imageContentStartPosition = contentStartPosition;
		InfoPopup.infoScrollRect = scrollRect;
		InfoPopup.toggleBtnText = toggleBtnTxt;
	}


	public void infoPopupShowAnimEvent(){
		InfoPopup.ShowPopup ();
	}


	
	public void InfoPopupToggle(){
		InfoPopup.PopupToggle ();
	}



	public void SwitchPopup(string content){
		InfoPopup.SwitchPopup (content);
	}
}
