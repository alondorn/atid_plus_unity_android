﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaneManager : MonoBehaviour {


	public static PlaneManager instance;

	public List<MissileHandler> missiles;

	int missileIndex = 0;

	public Text missilesBtnTxt;
	public Text wheelsBtnTxt;
	public Text fireBtnTxt;
	public Text specsBtnTxt;

	public Animator gearAnimator;
	public Animator planeUIAnimator;

	public bool wheelsDown = false;
	public bool wheelsMoving = false;
	public bool fireOn = false;

	public ParticleSystem fireParticles;

	public GameObject planeUiParent;

	public GameObject targetLight;





	void Awake(){
		instance = this;
	}



	public void PlaneStartEvent(){
		planeUiParent.SetActive (true);
		PopupsManager.instance.PlaneStartPopup ();
		//SpecsToggle (true);
		targetLight.SetActive (true);
	}



	public void PlaneEndEvent(){
		planeUiParent.SetActive (false);
		ResetPlane ();
		targetLight.SetActive (false);
	}


	bool specsOn = false;
	public void SpecsToggle(bool on){
		if (on && !specsOn) {
			planeUIAnimator.Play ("specs_in", -1, 0f);
			specsOn = true;
			specsBtnTxt.text = InfoPopup.ReverseString("הסתר נתונים");
		}else if (!on && specsOn) {
			planeUIAnimator.Play ("specs_out", -1, 0f);
			specsOn = false;
			specsBtnTxt.text = InfoPopup.ReverseString("הצג נתונים");
		}
	}

	public void SpecsToggle(){
		specsOn = !specsOn;
		if (!specsOn) {
			planeUIAnimator.Play ("specs_out", -1, 0f);
			specsBtnTxt.text = InfoPopup.ReverseString("הצג נתונים");
		} else {
			planeUIAnimator.Play ("specs_in", -1, 0f);
			specsBtnTxt.text = InfoPopup.ReverseString("הסתר נתונים");
		}
	}




	public void ResetPlane(){
		foreach (MissileHandler missile in missiles) {
			missile.Reset ();
		}
		missileIndex = 0;
		missilesBtnTxt.text = InfoPopup.ReverseString("שגר טיל");

		wheelsDown = false;
		wheelsBtnTxt.text = InfoPopup.ReverseString("הורד גלגלים");
		gearAnimator.Play ("gear_up_anim" , -1 , 0f);

		fireParticles.Stop ();
		fireOn = false;
		fireBtnTxt.text = InfoPopup.ReverseString("הפעל מבערים");


	}




	public void LounchMissile(){
		if (missileIndex == -1) {
			foreach (MissileHandler missile in missiles) {
				missile.Reset ();
			}
			missileIndex++;
			missilesBtnTxt.text = InfoPopup.ReverseString("שגר טיל");
		} else {
			missiles [missileIndex].Lounch ();
			missileIndex++;
			if (missileIndex >= 4) {
				missilesBtnTxt.text = InfoPopup.ReverseString("טען טילים");
				missileIndex = -1;
			}
		}
	}



	public void GearUpDown(){
		if (wheelsMoving)
			return;
		if (wheelsDown) {
			wheelsBtnTxt.text = InfoPopup.ReverseString("הורד גלגלים");
			gearAnimator.Play ("gear_up_anim" , -1 , 0f);
		} else {
			wheelsBtnTxt.text = InfoPopup.ReverseString("הרם גלגלים");
			gearAnimator.Play ("gear_down_anim" , -1 , 0f);
		}
	}
		



	public void FireToggle(){
		if (fireOn) {
			fireParticles.Stop ();
			fireOn = false;
			fireBtnTxt.text = InfoPopup.ReverseString("הפעל מבערים");
		} else {
			fireParticles.Play ();
			fireOn = true;
			fireBtnTxt.text = InfoPopup.ReverseString("כבה מבערים");
		}
	}




}
